#include "iostream"
#include "cstdio"

#include "example.h"

#define CATCH_CONFIG_MAIN
#include "../catch.hpp"

TEST_CASE("example", "example")
{
	REQUIRE(sum(5, 10) == 15);
}

TEST_CASE("failure", "should fail")
{
	REQUIRE(sum(1, 14) == 15);
}